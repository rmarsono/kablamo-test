// https://gist.github.com/vankasteelj/74ab7793133f4b257ea3
export const formattedSeconds = timeInSeconds => {
  const pad = function(num, size) {
      return ('000' + num).slice(size * -1)
    },
    time = parseFloat(timeInSeconds).toFixed(3),
    minutes = Math.floor(time / 60) % 60,
    seconds = Math.floor(time - minutes * 60),
    milliseconds = time.slice(-3)
  return pad(minutes, 2) + ':' + pad(seconds, 2) + '.' + pad(milliseconds, 3)
}
