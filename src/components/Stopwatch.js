import React, { Component } from 'react'

import Lap from './Lap'
import { formattedSeconds } from '../helpers'

import '../styles/styles.scss'

export default class App extends Component {
  state = {
    secondsElapsed: this.props.initialSeconds,
    lastClearedIncrementer: null,
    laps: []
  }

  start = 0

  lsRef = 'kablamo-stopwatch'

  componentDidMount() {
    const localStorageRef = localStorage.getItem(this.lsRef)
    if (localStorageRef) {
      this.setState(JSON.parse(localStorageRef))
      this.setState({ lastClearedIncrementer: null })
      this.incrementer = null
    }
  }

  componentDidUpdate() {
    localStorage.setItem(this.lsRef, JSON.stringify(this.state))
  }

  componentWillUnmount() {
    clearInterval(this.incrementer)
  }

  handleStartClick = () => {
    const { secondsElapsed } = this.state
    clearInterval(this.incrementer)
    if (secondsElapsed > 0) this.start = Date.now() - secondsElapsed * 1000
    else this.start = Date.now()
    this.incrementer = setInterval(() => {
      const delta = Date.now() - this.start
      this.setState({ secondsElapsed: delta / 1000 })
    }, 10)
  }

  handleStopClick = () => {
    clearInterval(this.incrementer)
    this.setState({
      lastClearedIncrementer: this.incrementer
    })
  }

  handleResetClick = () => {
    clearInterval(this.incrementer)
    this.start = 0
    this.setState({
      secondsElapsed: 0,
      laps: []
    })
  }

  handleLapClick = () => {
    let { laps } = { ...this.state }
    laps = laps.concat([this.state.secondsElapsed])
    this.setState({ laps })
  }

  handleDeleteClick = index => {
    let { laps } = { ...this.state }
    laps.splice(index, 1)
    this.setState({ laps })
  }

  render() {
    const { secondsElapsed, lastClearedIncrementer, laps } = this.state
    let buttonGroupClassName = 'button-group'
    if (secondsElapsed !== 0) buttonGroupClassName += ' has-two-buttons'
    return (
      <div className='stopwatch'>
        <h1 className='stopwatch-timer'>{formattedSeconds(secondsElapsed)}</h1>

        <div className={buttonGroupClassName}>
          {secondsElapsed === 0 ||
          this.incrementer === lastClearedIncrementer ? (
            <button
              type='button'
              className='start-btn'
              onClick={this.handleStartClick}
            >
              start
            </button>
          ) : (
            <button
              type='button'
              className='stop-btn'
              onClick={this.handleStopClick}
            >
              stop
            </button>
          )}
          {secondsElapsed !== 0 &&
          this.incrementer !== lastClearedIncrementer ? (
            <button type='button' onClick={this.handleLapClick}>
              lap
            </button>
          ) : null}

          {secondsElapsed !== 0 &&
          this.incrementer === lastClearedIncrementer ? (
            <button type='button' onClick={this.handleResetClick}>
              reset
            </button>
          ) : null}
        </div>

        <div className='stopwatch-laps'>
          {laps &&
            laps.map((lap, i) => (
              <Lap
                key={i}
                index={i + 1}
                lap={lap}
                onDelete={this.handleDeleteClick}
              />
            ))}
        </div>
      </div>
    )
  }
}
