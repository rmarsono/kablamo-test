import React from 'react'

import { formattedSeconds } from '../helpers'

const Lap = props => {
  const { index, lap, onDelete } = props
  return (
    <div key={index} className='stopwatch-lap'>
      <div className='time-group'>
        <span className='number'>{index}.</span>
        <span className='time'>{formattedSeconds(lap)}</span>
      </div>
      <button onClick={() => onDelete(index - 1)}> X </button>
    </div>
  )
}

export default Lap
