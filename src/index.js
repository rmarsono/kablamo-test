// Please detail what is wrong with the below code, and why. Also where applicable, mention what you would do differently.

import React from 'react'
import ReactDOM from 'react-dom'

import Stopwatch from './components/Stopwatch'

ReactDOM.render(
  <Stopwatch initialSeconds={0} />,
  document.getElementById('content')
)
